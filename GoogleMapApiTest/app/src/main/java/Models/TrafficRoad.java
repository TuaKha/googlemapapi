package Models;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Admin on 10/5/2018.
 */

public class TrafficRoad {
    private String placeId;
    private long speedLimit;
    private List<TrafficData> trafficDataList;

    public TrafficRoad(TrafficData trafficData, long speedLimit){
        this.placeId=trafficData.getPlaceId();
        this.speedLimit=speedLimit;
        trafficDataList=new ArrayList<>();
        trafficDataList.add(trafficData);
    }

    public String getPlaceId() {
        return placeId;
    }

    public long getSpeedLimit(){
        return speedLimit;
    }
    public List<TrafficData> getTrafficDataList() {
        return trafficDataList;

    }
}
