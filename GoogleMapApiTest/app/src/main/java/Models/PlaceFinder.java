package Models;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.googlemapapitest.MapActivity;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by Admin on 10/5/2018.
 */

public class PlaceFinder {
    private GeoDataClient mGeoDataClient;
    private PlaceInfo mPlace;


    public PlaceFinder(DirectionFinderListener listener, GeoDataClient mGeoDataClient) {
        this.mGeoDataClient = mGeoDataClient;
    }

    void getPlaceInfoById(String placeId) {
        mPlace = new PlaceInfo();
        mGeoDataClient.getPlaceById(placeId).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                if (task.isSuccessful()) {
                    PlaceBufferResponse places = task.getResult();
                    Place place = places.get(0);

                    try {
                        mPlace.setAddress(place.getAddress().toString());
                        //mPlace.setAttributions(place.getAttributions().toString());
                        mPlace.setId(place.getId().toString());
                        mPlace.setLatLng(place.getLatLng());
                        mPlace.setName(place.getName().toString());
                        mPlace.setPhoneNumber(place.getPhoneNumber().toString());
                        mPlace.setRating(place.getRating());
                        mPlace.setWebsiteUri(place.getWebsiteUri());
                    } catch (NullPointerException e) {
                        Log.e("Error: ", "onResult: NullPointerException: " + e.getMessage());
                    }
                    places.release();
                } else {
                    Log.e("Error: ", "Place not found.");
                }
            }
        });
    }
}
