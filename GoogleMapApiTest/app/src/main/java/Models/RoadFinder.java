package Models;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.admin.googlemapapitest.MapActivity;
import com.example.admin.googlemapapitest.R;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.RoadsApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.SnappedPoint;
import com.google.maps.model.SnappedSpeedLimitResponse;
import com.google.maps.model.SpeedLimit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Admin on 10/5/2018.
 */

public class RoadFinder {
    private GeoApiContext mContext;
    private MapActivity mapActivity;
    private TrafficData mTrafficData;
    private LatLng point;
    private float radius;
    private float currentVelocity;
    private int trafficLevel;
    private int duration;

    public RoadFinder(MapActivity mapActivity,float radius,float currentVelocity,int trafficLevel,int duration) {
        this.mapActivity=mapActivity;
        this.radius=radius;
        this.currentVelocity=currentVelocity;
        this.trafficLevel=trafficLevel;
        this.duration=duration;
        mContext = new GeoApiContext().setApiKey("AIzaSyAcXgGZ0d9ujapO3SMXvq5EeVG1Utb4wVI");
    }

    public RoadFinder(MapActivity mapActivity, TrafficData mTrafficData){
        this.mapActivity=mapActivity;
        this.mTrafficData=mTrafficData;
        mContext = new GeoApiContext().setApiKey("AIzaSyAcXgGZ0d9ujapO3SMXvq5EeVG1Utb4wVI");
    }

    /* GET PLACE ID */

    public void getPlaceIdByLatLng(LatLng point){
        this.point=new LatLng(point.latitude,point.longitude);
        Toast.makeText(mapActivity,"My lonngggg:"+Double.toString(point.longitude),Toast.LENGTH_LONG).show();
        mSnapToRoad.execute(point);
    }

    AsyncTask<LatLng, Integer, String> mSnapToRoad = new  AsyncTask<LatLng, Integer, String>() {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(LatLng... point) {
            try {
                //Log.d("current latitude",Double.toString(point[0].latitude));
                //Log.d("current longtitude",Double.toString(point[0].longitude));
                return snapToRoad(new LatLng(10.774320, 106.657242));
            } catch (final Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String placeId) {
            super.onPostExecute(placeId);
            //Log.d("My lat::::", Double.toString(point.latitude));
            //Log.d("My long::::", Double.toString(point.longitude));
            Toast.makeText(mapActivity, "My place ID:" + placeId, Toast.LENGTH_LONG).show();
            mapActivity.updateTraffic(new TrafficData(point, radius, currentVelocity, trafficLevel, duration, placeId));
            //return;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    };

    private String snapToRoad(LatLng point) throws Exception{
        List<com.google.maps.model.LatLng> mCapturedLocations= new ArrayList<>();
        mCapturedLocations.add(new com.google.maps.model.LatLng(point.latitude,point.longitude));
        com.google.maps.model.LatLng[] page = mCapturedLocations.subList(0,1).toArray(new com.google.maps.model.LatLng[1]);
        SnappedPoint[] snappedPoints = RoadsApi.snapToRoads(mContext, true, page).await();
        return snappedPoints[0].placeId;
    }

    /* GET SPEED LIMIT */
    public void getSpeedLimitByPlaceId(String placeId){
        mGetSpeedLimit.execute(placeId);
    }

    AsyncTask<String, Integer, Long> mGetSpeedLimit = new AsyncTask<String, Integer, Long>(){
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(String... placeId) {
            try {
                return getSpeedLimit(mContext, placeId[0]);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Long speedLimit) {
            super.onPostExecute(speedLimit);
            mapActivity.addTrafficRoad(new TrafficRoad(mTrafficData,speedLimit));
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    };


    private Long getSpeedLimit(GeoApiContext context, String placeId) throws Exception {
        Map<String, SpeedLimit> placeSpeeds = new HashMap<>();

        placeSpeeds.put(placeId, null);

        String uniquePlaceId=placeSpeeds.keySet().toString();

        SpeedLimit[] speedLimits = RoadsApi.speedLimits(context,placeId).await();

        placeSpeeds.put(speedLimits[0].placeId,speedLimits[0]);

        Log.d("My speeeed:",Long.toString(placeSpeeds.get(placeId).speedLimit));
        return placeSpeeds.get(placeId).speedLimit;
    }
}

