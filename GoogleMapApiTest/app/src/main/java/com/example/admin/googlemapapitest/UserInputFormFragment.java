package com.example.admin.googlemapapitest;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.GeoDataClient;

import Models.PlaceAutoCompleteAdapter;

/**
 * Created by Admin on 10/7/2018.
 */

public class UserInputFormFragment extends Fragment {
    float radius,velocity;
    int trafficLevel,secondDuration;
    EditText radiusEdt, velocityEdt;
    Spinner trafficLevelSpinner;
    NumberPicker hourPicker,minutePicker;
    Button submitBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.input_form_fragment, container, false);
    }

    // This event is triggered soon after onCreateView().
    // onViewCreated() is only called if the view returned from onCreateView() is non-null.
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        radiusEdt = view.findViewById(R.id.radius_edt);
        velocityEdt= view.findViewById(R.id.velocity_edt);
        trafficLevelSpinner = view.findViewById(R.id.traffic_spinner);

        //create a list of items for the spinner.
        String[] items = new String[]{"Low: 20-30 km/h", "Medium: 10-20 km/h", "High: 5-10 km/h"};
        //create an adapter to describe how the items are displayed, adapters are used in several places in android.
        ArrayAdapter<String> adapter =new ArrayAdapter<>(getContext(), R.layout.spinner_item, items);
        //set the spinners adapter to the previously created one.
        trafficLevelSpinner.setAdapter(adapter);

        hourPicker = view.findViewById(R.id.hourDuration);
        hourPicker.setMaxValue(6);
        hourPicker.setMinValue(0);

        minutePicker = view.findViewById(R.id.minDuration);
        minutePicker.setMaxValue(60);
        minutePicker.setMinValue(0);

        submitBtn = view.findViewById(R.id.usr_submit_button);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!radiusEdt.getText().toString().isEmpty()) {
                    radius = Float.parseFloat(radiusEdt.getText().toString());
                }
                else radiusEdt.setError("Please input radius!");

                if(!velocityEdt.getText().toString().isEmpty()) {
                    velocity = Float.parseFloat(velocityEdt.getText().toString());
                }
                else velocityEdt.setError("Please input velocity!");

                trafficLevel = trafficLevelSpinner.getSelectedItemPosition()+1;
                //if(hourPicker.getValue()==0 && minutePicker.getValue()==0)
                secondDuration=hourPicker.getValue()*3600 + minutePicker.getValue()*60;

                /*Toast.makeText(getActivity(),"Radius: "+Float.toString(radius)+
                        "\nVelocity: "+Float.toString(velocity)+
                        "\nTrafficLevel: "+ Integer.toString(trafficLevel)+
                        "\nDuration: "+Integer.toString(secondDuration),Toast.LENGTH_LONG).show();*/
                MapActivity mapActivity = (MapActivity)getActivity();
                mapActivity.getDeviceLocationForTrafficData(radius, velocity, trafficLevel, secondDuration);
            }
        });
    }

    // This method is called after the parent Activity's onCreate() method has completed.
    // Accessing the view hierarchy of the parent activity must be done in the onActivityCreated.
    // At this point, it is safe to search for activity View objects by their ID, for example.
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //String[] items = new String[]{"Low: 20-30 km/h", "Medium: 10-20 km/h", "High: 5-10 km/h"};
        //adapter =new ArrayAdapter<>(getActivity().getBaseContext(), R.layout.spinner_item, items);
        //dropdown.setAdapter(adapter);
    }
}
