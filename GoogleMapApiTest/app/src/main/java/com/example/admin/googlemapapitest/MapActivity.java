package com.example.admin.googlemapapitest;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.VoiceInteractor;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.RoadsApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.SnappedPoint;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import Models.DirectionFinder;
import Models.DirectionFinderListener;
import Models.PlaceAutoCompleteAdapter;
import Models.PlaceFinder;
import Models.PlaceInfo;
import Models.RoadFinder;
import Models.Route;
import Models.TrafficData;
import Models.TrafficRoad;

/**
 * Created by User on 10/2/2017.
 */

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, DirectionFinderListener {

    private static final String TAG = "MapActivity";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-40,-168), new LatLng(71,136));

    //vars
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlaceAutoCompleteAdapter mPlaceAutoCompleteAdapter;
    private GeoDataClient mGeoDataClient;
    private PlaceInfo mPlace;
    private ProgressDialog progressDialog;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();

    private List<TrafficRoad> mTrafficRoadList;
    private List<TrafficData> mTrafficDataList;

    //widgets
    private AutoCompleteTextView mSearchText;
    private ImageView mGuide,mGps,mMenu;

    public void sendGuideRequest(String destination){
        String origin= mSearchText.getText().toString();
        if(!origin.isEmpty()){
            try{
                new DirectionFinder(MapActivity.this,origin,destination).execute();
            }catch (UnsupportedEncodingException e){
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(MapActivity.this, "Please enter origin",Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        Toast.makeText(MapActivity.this,"FIND SUCCESS!",Toast.LENGTH_LONG).show();
        /*for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }*/

        //Consider first route only
        Route firstRoute= routes.get(0);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(firstRoute.startLocation, 16));

        originMarkers.add(mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination))
                .title(firstRoute.startAddress)
                .position(firstRoute.startLocation)));
        destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination))
                .title(firstRoute.endAddress)
                .position(firstRoute.endLocation)));

        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.BLUE).
                width(10);

        for (int i = 0; i < firstRoute.points.size(); i++)
            polylineOptions.add(firstRoute.points.get(i));

        polylinePaths.add(mMap.addPolyline(polylineOptions));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is Ready", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onMapReady: map is ready");
        mMap = googleMap;
        updateLocationUI();
        getDeviceLocation();
        init();
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mTrafficRoadList = new ArrayList<>();
        mTrafficDataList = new ArrayList<>();

        mSearchText=(AutoCompleteTextView) findViewById(R.id.input_search);

        mGuide =  findViewById(R.id.ic_direction);

        mGps =  findViewById(R.id.ic_gps);

        mMenu = findViewById(R.id.ic_menu);

        initMap();
    }

    private void init(){
        Log.d(TAG, "init: initializing");

        mGeoDataClient = Places.getGeoDataClient(this, null);

        mPlaceAutoCompleteAdapter=new PlaceAutoCompleteAdapter(MapActivity.this, mGeoDataClient, LAT_LNG_BOUNDS, null);

        mSearchText.setAdapter(mPlaceAutoCompleteAdapter);

        mSearchText.setOnItemClickListener(mAutocompleteClickListener);

        mSearchText.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH
                                || actionId == EditorInfo.IME_ACTION_DONE
                                || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                                || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {
                            //Execute searching method
                            geoLocate();
                        }
                        return false;
                    }
                });

        mGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction mFragmentTransaction=getFragmentManager().beginTransaction();
                DestinationFragment desFragment = new DestinationFragment();

                desFragment.setPlaceAutoCompleteAdapter(mPlaceAutoCompleteAdapter);
                //Bundle bundle = new Bundle();
                //bundle.putString("My name","ABS");
                //bundle.putInt("My age",15);
                //desFragment.setArguments(bundle);

                mFragmentTransaction.replace(R.id.destFragmentContainer, desFragment, "FragDes");
                mFragmentTransaction.commit();
            }
        });

        mGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MapActivity.this, "Back to device location", Toast.LENGTH_SHORT).show();
                getDeviceLocation();
            }
        });

        mMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction mFragmentTransaction=getFragmentManager().beginTransaction();
                UserInputFormFragment inputFormFragment = new UserInputFormFragment();

                mFragmentTransaction.replace(R.id.inputFormFragmentContainer, inputFormFragment, "FragInputForm");
                mFragmentTransaction.commit();
            }
        });
    }

    private void geoLocate(){
        Log.d(TAG, "geoLocate: geolocating");

        String searchString = mSearchText.getText().toString();

        Geocoder geocoder= new Geocoder(MapActivity.this);

        List<Address> list= new ArrayList<>();
        try{
            //first parameter: search text for finding location, second parameter: maximum number of result get
            list=geocoder.getFromLocationName(searchString,1);

        }catch (IOException e){
            Log.e(TAG,"geoLocate: IOException: " + e.getMessage());
        }

        if(list.size() > 0){
            Address address = list.get(0);

            Log.d(TAG,"geoLocate: found a location: "+address.toString());
            moveCamera(new LatLng(address.getLatitude(),address.getLongitude()),DEFAULT_ZOOM,address.getAddressLine(0));
        }
    }


    private void moveCamera(LatLng latLng, float zoom, String title){
        Log.d(TAG, "moveCamera: moving the camera to lat: "+latLng.latitude+ ", lng: "+latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        //Add a marker
        MarkerOptions marker=new MarkerOptions().position(latLng).title(title);
        mMap.addMarker(marker);

        hideKeyboard(this);
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionsGranted) {
                mMap.setMyLocationEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                initMap();
            }
            //mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    //Get device location and move camera to the location
    private void getDeviceLocation(){
        Log.d(TAG,"getDeviceLocation: getting the device current location");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: location found!");

                            //get location
                            Location currentLocation = (Location) task.getResult();

                            LatLng locationLatLng=new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());

                            Log.d(TAG, "moveCamera: moving the camera to lat: "+locationLatLng.latitude+ ", lng: "+locationLatLng.longitude);

                            //move camera to location
                        } else {
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(MapActivity.this, "unable to get location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e){
            Log.e(TAG, "getDeviceLocation: SecurityException: "+e.getMessage());
        }
    }

    private void initMap(){
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(MapActivity.this);

        //mMap.setOnMapClickListener(....);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    /*
       ----------------------------RETRIEVE PLACE INFO---------------------------
    */

    private AdapterView.OnItemClickListener mAutocompleteClickListener=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            hideKeyboard(MapActivity.this);

            final AutocompletePrediction item = mPlaceAutoCompleteAdapter.getItem(position);

            final String placeId=item.getPlaceId();

            getPlaceInfoById(placeId);
        }
    };

    void getPlaceInfoById(String placeId){
        mGeoDataClient.getPlaceById(placeId).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                if (task.isSuccessful()) {
                    PlaceBufferResponse places = task.getResult();
                    Place place= places.get(0);

                    mPlace = new PlaceInfo();

                    try{
                        mPlace.setAddress(place.getAddress().toString());
                        //mPlace.setAttributions(place.getAttributions().toString());
                        mPlace.setId(place.getId().toString());
                        mPlace.setLatLng(place.getLatLng());
                        mPlace.setName(place.getName().toString());
                        mPlace.setPhoneNumber(place.getPhoneNumber().toString());
                        mPlace.setRating(place.getRating());
                        mPlace.setWebsiteUri(place.getWebsiteUri());
                    }catch (NullPointerException e){
                        Log.e(TAG, "onResult: NullPointerException: " + e.getMessage() );
                    }

                    places.release();
                    Toast.makeText(MapActivity.this,"Name:"+mPlace.getName()
                            +"\nAddress:"+mPlace.getAddress()
                            +"\nPhoneNumber:"+mPlace.getPhoneNumber(),Toast.LENGTH_LONG).show();
                } else {
                    Log.e(TAG, "Place not found.");
                    Toast.makeText(MapActivity.this,"NOT FOUND",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*
       ----------------------------HANDLE UPLOAD TRAFFIC DATA---------------------------
    */
    public void getDeviceLocationForTrafficData(final float radius, final float currentVelocity, final int trafficLevel, final int duration ){
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            //get location
                            Location currentLocation = (Location) task.getResult();

                            LatLng locationLatLng=new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());

                            RoadFinder roadFinder= new RoadFinder(MapActivity.this,radius,currentVelocity,trafficLevel,duration);
                            roadFinder.getPlaceIdByLatLng(locationLatLng);

                            /*TrafficData mTrafficData= new TrafficData(locationLatLng,radius,currentVelocity,trafficLevel,duration,placeId);

                            mTrafficDataList.add(mTrafficData);*/
                        } else {
                            Log.d(TAG, "onComplete: current location is null");
                        }
                    }
                });
            }
        } catch (SecurityException e){
            Log.e(TAG, "getDeviceLocation: SecurityException: "+e.getMessage());
        }
    }

    public void updateTraffic(TrafficData mTrafficData){
        Toast.makeText(MapActivity.this,
                "Longtitude: "+Double.toString(mTrafficData.getCenter().longitude)
                +"\nPlaceId: "+mTrafficData.getPlaceId()
                +"\nRadius: "+Float.toString(mTrafficData.getRadius())
                +"\nVelocity: "+Float.toString(mTrafficData.getCurrentVelocity())
                +"\nTrafficLevel: "+ Integer.toString(mTrafficData.getTrafficLevel())
                +"\nDuration: "+Integer.toString(mTrafficData.getDuration()),Toast.LENGTH_LONG).show();

        boolean flag=true;
        for(int i=0;i<mTrafficRoadList.size();i++) {
            if(mTrafficData.getPlaceId().equals(mTrafficRoadList.get(i).getPlaceId())){
                mTrafficRoadList.get(i).getTrafficDataList().add(mTrafficData);
                flag=false;
            }
        }

        if(flag) {
            new RoadFinder(MapActivity.this,mTrafficData).getSpeedLimitByPlaceId(mTrafficData.getPlaceId());
        }
        else{
            //Call Function A
        }
    }


    public void addTrafficRoad(TrafficRoad trafficRoad){
        mTrafficRoadList.add(trafficRoad);
        Toast.makeText(MapActivity.this,Long.toString(trafficRoad.getSpeedLimit()),Toast.LENGTH_LONG).show();
        getPlaceInfoById(trafficRoad.getPlaceId());
        //Call Function A
        //mGetPlaceId.execute();
    }


    public String getPlaceId(){
        GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBd_eCtuatIdmDbltS6jDrjAMSXuvaGFGU");
        GeocodingResult[] results = new GeocodingResult[0];
        try {
            results = GeocodingApi.newRequest(context)
                    .latlng(new com.google.maps.model.LatLng(52.2641, 76.9597)).await();
            Toast.makeText(MapActivity.this,results[0].placeId,Toast.LENGTH_LONG).show();
            Log.d("====",results[0].placeId);
            return results[0].placeId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    AsyncTask<Void,Void,String> mGetPlaceId = new AsyncTask<Void, Void, String>(){
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... placeId) {
            try {
                return getPlaceId();
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String placeId) {
            super.onPostExecute(placeId);
            Log.d("Place id: ",placeId);
        }
    };
}











